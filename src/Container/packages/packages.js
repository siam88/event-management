import React, { Component, useState } from "react";
import Slider from "../../components/ImgSlider/ImgSlider";
import classes from "./packages.module.css";
import cover from "../../assets/images/cover.jpg";
import Aux from "../../hoc/Auxs";
import Footer from "../../components/Footer/footer";
import TeamSlider from "../../components/TeamSlider/teamSlider";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.min.css";
import "owl.carousel/dist/assets/owl.theme.default.min.css";
import Card from "../../components/UI/Card/card";
import 'antd/dist/antd.css';

import { Form, Input, Button, Space, Select } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';


const { Option } = Select;
const areas = [
  { label: 'Dhaka', value: 'Dhaka' },
  { label: 'Gazipur', value: 'Gazipur' },
  { label: 'Mymensingh', value: 'Mymensingh' },
  { label: 'Barishal', value: 'Barishal' },

];



const sights = {
  Dhaka: ['Uttara', 'Banani'],
  Gazipur: ['Tongi', 'Collegegate'],
};



const Homepage = () => {

  const [form] = Form.useForm();
  const [count, setCount] = useState(false);


  const onFinish = values => {
    console.log('Received values of form:', values);
    setCount(true);
    console.log(count);
  };

  const handleChange = () => {
    form.setFieldsValue({ sights: [] });
  };
  return (
    <section>
      <div className={classes.cartBanner}>
        <div class="container cartTitle">
          <h1 style={{ paddingTop: "5%" }}>Packages</h1>
          <p class="path"></p>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 my-5">
            <div class={classes.pricingTable}>
              <div class={classes.pricingTableHeader}>
                <h3 class={classes.title}>Create Your Own Package for best deal </h3>
                <div class={classes.priceValue}> <Form form={form} name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
                  <Form.Item name="area" label="Area" rules={[{ required: true, message: 'Missing area' }]}>
                    <Select options={areas} onChange={handleChange} />
                  </Form.Item>
                  <Form.List name="sights">
                    {(fields, { add, remove }) => (
                      <>
                        {fields.map(field => (
                          <Space key={field.key} align="baseline">
                            <Form.Item
                              noStyle
                              shouldUpdate={(prevValues, curValues) =>
                                prevValues.area !== curValues.area || prevValues.sights !== curValues.sights
                              }
                            >
                              {() => (
                                <Form.Item
                                  {...field}
                                  label="Sight"
                                  name={[field.name, 'sight']}
                                  fieldKey={[field.fieldKey, 'sight']}
                                  rules={[{ required: true, message: 'Missing sight' }]}
                                >
                                  <Select disabled={!form.getFieldValue('area')} style={{ width: 130 }}>
                                    {(sights[form.getFieldValue('area')] || []).map(item => (
                                      <Option key={item} value={item}>
                                        {item}
                                      </Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                              )}
                            </Form.Item>
                            <Form.Item
                              {...field}
                              label="Budget"
                              name={[field.name, 'price']}
                              fieldKey={[field.fieldKey, 'price']}
                              rules={[{ required: true, message: 'Missing price' }]}
                            >
                              <Input />
                            </Form.Item>

                            <MinusCircleOutlined onClick={() => remove(field.name)} />
                          </Space>
                        ))}

                        <Form.Item>
                          <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                            Add sights
              </Button>
                        </Form.Item>
                      </>
                    )}
                  </Form.List>
                  <Form.Item>
                    <Button type="primary" htmlType="submit">
                      Submit
        </Button>
                  </Form.Item>
                </Form></div>
              </div>
              <div class={classes.pricingContent}>
                {count?<><ul>
                  <li>Holud Stage</li>
                  <li>50pcs Chair</li>
                  <li>Dance Stage</li>
                  <li>Photographer: 1 Top Photographer</li>
                  <li>Event Duration: 4 Hours</li>
                  <li>Number of Pictures: Unlimited (All post processed)</li>
                  <li>Specially Edited Photos: 100 copies</li>
                  <li>Print: 4R (4”x6″) Matte Prints: 100 copies</li>
                </ul> <div class={classes.pricingTableSignup}>
                  <a href="#">Process to order</a>
                </div></>:null}
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 my-5">
            <div class={classes.pricingTable}>
              <div class={classes.pricingTableHeader}>
                <h3 class={classes.title}>SUPER SAVER </h3>
                <div class={classes.priceValue}>TK.20,000/=</div>
              </div>
              <div class={classes.pricingContent}>
                <ul>
                  <li>Holud Stage</li>
                  <li>50pcs Chair</li>
                  <li>Dance Stage</li>
                  <li>Photographer: 1 Top Photographer</li>
                  <li>Event Duration: 4 Hours</li>
                  <li>Number of Pictures: Unlimited (All post processed)</li>
                  <li>Specially Edited Photos: 100 copies</li>
                  <li>Print: 4R (4”x6″) Matte Prints: 100 copies</li>
                </ul>
                <div class={classes.pricingTableSignup}>
                  <a href="#">Sign Up</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 my-5">
            <div class={classes.pricingTable}>
              <div class={classes.pricingTableHeader}>
                <h3 class={classes.title}>Premium</h3>
                <div class={classes.priceValue}>TK.50,000/=</div>
              </div>
              <div class={classes.pricingContent}>
                <ul>
                  <li>
                    Photographer: Top 2 photographer & 1 Cinematographer.
                    </li>
                  <li>Wedding Stage & Sound system</li>
                  <li>Holud Stage</li>
                  <li>100pcs Chair</li>
                  <li>Dance Stage</li>
                  <li>Event Duration: 4 Hours</li>
                  <li>Number of Pictures: Unlimited (All post processed)</li>
                  <li>Specially Edited Photos: 200 copies</li>
                </ul>
                <div class={classes.pricingTableSignup}>
                  <a href="#">Sign Up</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );

}
export default Homepage;
